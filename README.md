# Truck-Sales-and-Ordering-system



What is it?
-----------

Full featured Django/python system for entering client orders and generating relevant documents for processing sales

Features: 
Enter Customer requests, 
Generate Proforma Invoice Document (PDF), 
Generate QSF  Document (PDF), 
Generate Calculation  Document (PDF), 
Register customer and clients 
and much more...


Licensing
  ---------

Please see the file called LICENSE.

Contacts
  --------